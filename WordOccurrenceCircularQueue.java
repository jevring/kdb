
/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 * 
 * <ul>
 * 
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 * 
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 * 
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 * 
 * </ul>
 * 
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import java.io.Serializable;

/**
 * A special version of the CircularQueue to handle WordOccurrences.
 *
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-18
 */
public class WordOccurrenceCircularQueue extends CircularQueue implements Serializable {

    /**
     * Constructs the queue.
     *
     * @param size size of the queue
     * @see CircularQueue#CircularQueue(int)
     */
    public WordOccurrenceCircularQueue(int size) {
        super(size);
    }

    /**
     * Takes a WordOccurrence and sets it's words.
     *
     * @param wo the WordOccurrence to populate.
     */
    public void fillWordOccurrence(WordOccurrence wo) {
        int numClosestWords = (size - 1) / 2;

        String[] beforeStringArray = new String[numClosestWords];
        String[] afterStringArray = new String[numClosestWords];
        for (int i = 0; i < numClosestWords; i++) {
            Object opre = listBefore.get(i);
            if (opre instanceof String) {
                beforeStringArray[i] = (String) opre;
            } else {
                // then it MUST be an instance of WordOccurrence, since those are the only ones we add.
                beforeStringArray[i] = ((WordOccurrence)listBefore.get(i)).getWord();
            }
            Object opost = listAfter.get(i);
            if (opost instanceof String) {
                afterStringArray[i] = (String) opost;
            } else {
                // then it MUST be an instance of WordOccurrence, since those are the only ones we add.
                afterStringArray[i] = ((WordOccurrence)listAfter.get(i)).getWord();
            }
        }
        wo.setWordsBefore(beforeStringArray);
        wo.setWordsAfter(afterStringArray);
    }
}
