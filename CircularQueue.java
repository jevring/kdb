
/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 * 
 * <ul>
 * 
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 * 
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 * 
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 * 
 * </ul>
 * 
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */
import java.util.LinkedList;
import java.io.Serializable;
/**
 * A Circular Queue with a fixed size. Like a moving window. push on one side, it automatically pops on the other side.<br>
 * Interesting for caching objects around the middle object in the queue
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-18
 */
public class CircularQueue implements Serializable {
    LinkedList listBefore;
    LinkedList listAfter;
    Object currentObject;
    int size;

    /**
     * Constructs the main queue structure and fills it with null-tokens.
     *
     * @param size the size of the queue, usually (n * 2 + 1) where n is the cachesize you need on both sides of the object.
     */
    public CircularQueue(int size) {
        this.size = size;
        init();
    }

    /**
     * Handles the actual enqueing and moving of the window.
     *
     * @param o the object to be added to the queue
     */
    public void enque(Object o) {
        // NOTE: add from the RIGHT side, as to emulate the text being seen as through a "moving window"

        // add to the end of the queue (scrolling in as the window moves)
        listAfter.addLast(o);
        // push middle
        listBefore.addLast(currentObject);
        // pop middle from list
        currentObject = listAfter.removeFirst();
        // trim list
        listBefore.removeFirst();
    }

    /**
     * Gets the list of objects to the left (the pop side) of the middle object.
     *
     * @return a list of (size - 1) / 2 objects
     */
    public LinkedList getPreList() {
        return listBefore;
    }

    /**
     * Gets the list of objects to the left /the push side) of the middle object.
     *
     * @return a list of (size - 1) / 2 objects
     */
    public LinkedList getPostList() {
        return listAfter;
    }

    /**
     * Gets the middle (main) object from the queue.
     *
     * @return the middle object
     */
    public Object getCurrent() {
        return currentObject;
    }

    /**
     * Clears the queue, and initializes itrfrom scratch with the help of <code>init()</code>.
     *
     */
    public void clear() {
        init();
    }

    /**
     * Does the initialization of the queue, either when just created, or when cleared. Fills queue with null tokens. 
     */
    private void init() {
        listBefore = new LinkedList();
        currentObject = "$$$";
        listAfter = new LinkedList();
        for (int i = 0; i < (size - 1) / 2; i++) {
            listBefore.addLast("$$$");
            listAfter.addLast("$$$");
        }
    }
}
