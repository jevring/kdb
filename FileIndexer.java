
/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 * 
 * <ul>
 * 
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 * 
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 * 
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 * 
 * </ul>
 * 
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

/**
 * A special case of TKD for handling files as chapters. One file = one chapter
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-19
 */
public class FileIndexer extends Indexer {
    private TKD tkd;
    private String file;
    private int totalWordCount = 0;
    private int totalLineCount = 0;
    private int sectionWordCount = 0;
    private int sectionLineCount = 0;
    private int sectionUniqueWordCount = 0;
    private int totalUniqueWordCount = 0;
    private boolean cacheChapter;


    /**
     * Constructs the <code>FileIndexer</code> with the proper attributes to support the rest of the functions.
     *
     * @param tkd the <code>TKD</code> (can be any type) that will house the information
     * @param numClosestWords the number of words to be indexed
     * @param filename the filename of the file we wish to index
     */
    public FileIndexer(TKD tkd, int numClosestWords, String filename, boolean cacheChapter) {
        super(tkd.getTree(), numClosestWords);
        this.tkd = tkd;
        this.file = filename;
        this.cacheChapter = cacheChapter;
    }

    /**
     * Reads the previously specified file from disk, and indexes it into the tree.
     */
    public void read() {
        WordOccurrence wo;
        // caches the chapter based on what we tell the FileIndexer
        Chapter chapter = new Chapter(file, tkd, cacheChapter);
        wordReferences = new HashMap();
        File f = new File(file);
        String line;
        Section section = new Section();
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(f));
            while ((line = bfr.readLine()) != null) {
                totalLineCount++;
                sectionLineCount++;
                //System.out.println("totalLineCount: " + totalLineCount + " sectionLineCount: " + sectionLineCount);
                //single empty line by itself means the beginning of a new section
                if (line.equals("")) {
                    // PROBLEM: the last section doesn't get the special treatment, or any kind of treatment for that matter.
                    section = closeOldSectionAndCreateNew(section, chapter, sectionLineCount, totalLineCount, sectionWordCount, sectionUniqueWordCount);

                    wocq.clear();
                } else {
                    StringTokenizer st = new StringTokenizer(line);
                    while(st.hasMoreTokens()) {
                        String word = st.nextToken();
                        // TODO: check words for a trailing "-", and if they have one, concatenate the word with the first word of the next line (version 2.0)

                        if (!stopWords.contains(word) && word.length() > 1) {
                            sectionWordCount++;
                            totalWordCount++;

                            // makes sure only real words, and strings of digits are added.
                            word = clean(word);
                            WordReference wr;
                            // you CAN speed this up by not checking this, and just getting it and checking for null later instead. (and cast it from object when it's != null)
                            if (wordReferences.containsKey(word)) {
                                wr = (WordReference)wordReferences.get(word);
                            } else {
                                sectionUniqueWordCount++;
                                totalUniqueWordCount++;
                                wr = new WordReference(word);
                                wordReferences.put(word, wr);
                            }
                            // always create a WordOccurrence. those are the only interesting ones. the WordReferences are only the keepers of the WordOccurrences.
                            // the handlers of the one-to-many relationship
                            wo = new WordOccurrence(section, wr);
                            wo.setSectionStartPosition(sectionWordCount);
                            wo.setChapterStartPosition(totalWordCount);
                            wr.addOccurance(wo);

                            wocq.enque(wo);
                            // add the WordOccurrences to the list, and when looking at a certain WordOccurrence, set pre and post words for the WordOccurrence n steps back
                            // that which will be the "currentObject" in the queue

                            // we're now at the END of the CircularQueue, now do stuff for the CURRENTWORD in the queue
                            if (wocq.getCurrent() instanceof WordOccurrence)  {
                                wocq.fillWordOccurrence((WordOccurrence)wocq.getCurrent());
                            } // if not, then this isn't interesting, just skip to the next word

                            // NOTE: when we've done everything for the l-n objects, we need to do the same for the n last objects!
                        }
                    }
                }
                // this information might come in handy later
                // it's ok to change the chapter here, since it's an object reference.
                chapter.setTotalLineCount(totalLineCount);
                chapter.setTotalUniqueWordCount(totalUniqueWordCount);
                chapter.setTotalWordCount(totalWordCount);
            }
            // call this one last time to catch the very last section
            closeOldSectionAndCreateNew(section, chapter, sectionLineCount, totalLineCount, sectionWordCount, sectionUniqueWordCount);
            Iterator i = wordReferences.keySet().iterator();
            while (i.hasNext()) {
                WordReference wr = (WordReference)wordReferences.get(i.next());
                tree.insert(wr.getWord(), wr);
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println("The system could not find the file specified: " + file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Helper method to clear up the read() method clutter. Nothing to see here folks, move along.
     * @param section
     * @param chapter
     * @param sectionLineCount
     * @param totalLineCount
     * @param sectionWordCount
     * @param sectionUniqueWordCount
     * @return a new setion with startpoints and some other stuff set.
     */
    private Section closeOldSectionAndCreateNew(Section section, Chapter chapter, int sectionLineCount, int totalLineCount, int sectionWordCount, int sectionUniqueWordCount) {
        for (int i = 0; i < numClosestWords; i++) {
            // loop over and push the last words of the section into the "current" position in the queue
            wocq.enque("$$$");
            if (wocq.getCurrent() instanceof WordOccurrence)  {
                wocq.fillWordOccurrence((WordOccurrence)wocq.getCurrent());
            } // if not, then this isn't interesting, just skip to the next word
        }
        // NOTE: they MUST be in this order. otherwise, the sections gets the wrong length, and the wrong startingposition.
        // set stuff for the previous section, sutuff that can only be known when we've gone over the entire section
        section.setLinesInSection(sectionLineCount-1);
        section.setWordsInSection(sectionWordCount);
        section.setUniqueWordCount(sectionUniqueWordCount);
        section = new Section();
        // set stuff for the new section.        
        section.setChapter(chapter);
        section.setStartingLineInChapter(totalLineCount);
        this.sectionWordCount = 0;
        this.sectionLineCount = 0;
        this.sectionUniqueWordCount = 0;
        return section;
    }

    /**
     * Cleans the word we're about to add from unwanted chars such as ?!)(/&%�#".<br>
     * This is called for EVERY word we index, so it has to be lightning fast, which it is.
     * 
     * @param word the word to be cleaned
     * @return a cleaned version of the word.
     */
    private String clean(String word) {
        // use this to trim stuff like " and other small stuff in the beginning and end of the word

        int length = word.length();
        char[] newWord = new char[ length ];
        int j = 0;
        int i;
        for (i = 0; i < length; i++) {
            char c = word.charAt(i);
            if (Character.isLetterOrDigit(c)) {
                newWord[ j++ ] = c;
            }
        }
        String t = new String( newWord, 0, j );
        return t.toLowerCase();
    }
}
