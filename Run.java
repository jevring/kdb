/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */
/**
 * The Tester class that runs the whole show currently.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 0.1 2004-05-13
 * @version 2.0 2004-05-26
 */
public class Run {
    public static void main(String[] args) {
        new GUI();
        /*
        TrieTree tree = new TrieTree();
        GUITest gui = new GUITest(tree);
        Indexer ia = new Indexer(tree);
        ia.readFile("C:\\_data\\download\\!documents\\devilsdictionary.txt");
        */

        //Date dd = new Date();
        //System.out.println("Start: 0");
        // NOTE: REMEMBER, objects are just like tables.! (sometimes atleast)
/*
        System.out.println(System.currentTimeMillis());
        FileTKD tkd = new FileTKD("Devils Dictionary", "Description");
        //DirTKD tkd = new DirTKD("The Smaller Bible", "A small subset of the bible, cirka 1MB of files, 1/4 of the bible");
        System.out.println(System.currentTimeMillis());

        //tkd.createIndex("C:\\_data\\download\\code\\bible_small\\", 3);
        tkd.createIndex("C:\\_data\\download\\!documents\\devilsdictionary.txt", 3);
        System.out.println(System.currentTimeMillis());

        //KDB kdb = new KDB(tkd);
        // fungerar ox� med:, dom returnerar samma sak.
        KDB kdb = tkd.createKDB();
        System.out.println(System.currentTimeMillis());

        // this search is executed in VERY little time. the only thing that actually takes some reasources s the indexing.
        Object o = kdb.searchWord("rebuke");
        // To print an entire chapter, do wo.getSection().getChapter(), create an iterator for .getSections(), run over that, and print each section.
        // GUI notes: partialListHits should have the stem marked in blue or bold or something. every hit should have a "display chapter" and "display section" button or link next to it.
        System.out.println(System.currentTimeMillis());
        if (o != null) {
            // usually (always) only one! (this is the WordReference <- the WordReference is actually just there so that the TrieTree can contain other stuff aswell. otherwise, the WordOccurrences might aswell have been saved directly in the TrieTree nodes.)
            Iterator i = ((Collection)o).iterator();
            Section section = null;
            while (i.hasNext()) {
                Collection c = ((WordReference)i.next()).getOccurances();
                Iterator j = c.iterator();
                while (j.hasNext()) {
                    WordOccurrence wo = (WordOccurrence)j.next();
                    // do this check below for unique section traversal
                    System.out.println("---");
                    if (section != wo.getSection()) {
                        for (int k = 0; k < wo.getWordsBefore().length; k++) {
                            String s = wo.getWordsBefore()[k];
                            System.out.print(s + " ");
                        }
                        System.out.print(" : " + wo.getWord() + " : ");
                        for (int l = 0; l < wo.getWordsAfter().length; l++) {
                            String s = wo.getWordsAfter()[l];
                            System.out.print(s + " ");
                        }
                        System.out.println("\n- SECTION TEXT -----");
                        System.out.println(wo.getSection().getSectionText());
                        System.out.println("---");
                        section = wo.getSection();
                    }
                }
            }
        } else {
            System.out.println("word not found");
        }
        tkd.saveToFile("tkds/" + tkd.getName() + ".tkd");
*/
        /*
        TrieTree t = new TrieTree();
        t.insert("ske", "1");
        t.insert("sked", "2");
        t.insert("sena", "3");
        t.insert("senap", "4");
        t.insert("senapsfr�", "4");
        t.insert("senig", "4");
        t.insert("sensation", "5");

        Object o = t.searchString("sena");
        if (o != null) {
            Collection l = (Collection)o;
            Iterator i = l.iterator();
            while (i.hasNext()) {
                TrieTree trieTree = (TrieTree) i.next();
                System.out.println( trieTree.getNodeWord());
            }

        }
        */
        /*
        Iterator i = t.searchWord("sak").iterator();
        while (i.hasNext()) {
            TrieEntry te = (TrieEntry)i.next();
            System.out.println(te);
        }
        */


        /*
        Iterator it = t.searchString("t").iterator();
        while (it.hasNext()) {
            String s = (String)it.next();
            System.out.println(s);
        }
        */

    }
}
