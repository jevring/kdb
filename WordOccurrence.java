
/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import java.io.Serializable;

/**
 * Contains pointers to where in a section and chapter this word can be found.<br>
 * If we have a WordReference of "cat", it will contain several WordOccurrences that say that the word "cat" can be found and such-and-such place(s) in the setcion text.<br>
 * A <code>WordReference</code> contains a number of these.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-13
 */
public class WordOccurrence implements Serializable{
    // NOTE: don't have the word here aswell, there's no need. we need to keep everyobject as small as possible.
    private Section section;
    // do NOT make this public, or allow it to be get:ed, since then we can end up in infinite .get(..) loops.
    private WordReference wr;
    private String[] wordsBefore;
    private String[] wordsAfter;
    private int sectionStartPosition;
    private int chapterStartPosition;

    /**
     * Constructs a WordOccurrence, and ties it to a <code>Section</code> and a <code>WordReference</code>.
     *
     * @param section section to tie this occurance to
     * @param wr reference this occurance IS tied to
     */
    public WordOccurrence(Section section, WordReference wr) {
        this.section = section;
        this.wr = wr;
        //default to 3 which is the value most often used.
        wordsBefore = new String[3];
        wordsAfter = new String[3];
    }

    // getters and setters, no need to javadoc those
    public Section getSection() {
        return section;
    }

    // NOTE: setSection() must NOT be implemented, since changing the section invalidates the design.
    public String[] getWordsBefore() {
        return wordsBefore;
    }

    public void setWordsBefore(String[] wordsBefore) {
        this.wordsBefore = wordsBefore;
    }

    public String[] getWordsAfter() {
        return wordsAfter;
    }

    public void setWordsAfter(String[] wordsAfter) {
        this.wordsAfter = wordsAfter;
    }

    /**
     * Gets the word this occurance describes, by asking the WordReference it's connected to to tell us about its word.
     *
     * @return the word this occurance represents
     */
    public String getWord() {
        return wr.getWord();
    }

    // getters and setters, no need to javadoc those
    public int getSectionStartPosition() {
        return sectionStartPosition;
    }

    public void setSectionStartPosition(int sectionStartPosition) {
        this.sectionStartPosition = sectionStartPosition;
    }

    public int getChapterStartPosition() {
        return chapterStartPosition;
    }

    public void setChapterStartPosition(int chapterStartPosition) {
        this.chapterStartPosition = chapterStartPosition;
    }

    public Chapter getChapter() {
        return this.getSection().getChapter();
    }

    /**
     * A String representation. returns string with the words before and the word, and then the ones after.
     *
     * @return a "hit" definition with words
     * @see #getWord() 
     *
     */
    public String toString() {
        String s = "";
        for (int i = 0; i < wordsBefore.length; i++) {
            String s1 = wordsBefore[i];
            if (!s1.equals("$$$")) {
                s += s1 + " ";
            }
        }
        s += wr.getWord();
        for (int i = 0; i < wordsAfter.length; i++) {
            String s1 = wordsAfter[i];
            if(!s1.equals("$$$")) {
                s += " " + s1;
            }
        }
        return s;
    }
}
