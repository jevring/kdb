/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */
import java.io.Serializable;
/**
 * Section objects contain information about a section in a chapter, what chapter it belongs to, where it starts and ends and such.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-13
 */
public class Section implements Serializable {
    private Chapter chapter;

    private int startingLineInChapter = 0;
    private int linesInSection = 0;
    private int wordsInSection = 0;
    private int uniqueWordCount = 0;

    /**
     * Constructs a basic section. <br>
     * We can't use avariable-ridden constructor, since almost half of the valurs must be set at a much later stage.
     */
    public Section() {}

    // Getters and setters, no need to javadoc those
    public int getUniqueWordCount() {
        return uniqueWordCount;
    }

    public void setUniqueWordCount(int uniqueWordCount) {
        this.uniqueWordCount = uniqueWordCount;
    }

    public int getStartingLineInChapter() {
        return startingLineInChapter;
    }

    public void setStartingLineInChapter(int startingLineInChapter) {
        this.startingLineInChapter = startingLineInChapter;
    }

    public int getLinesInSection() {
        return linesInSection;
    }

    public void setLinesInSection(int linesInSection) {
        this.linesInSection = linesInSection;
    }

    public int getWordsInSection() {
        return wordsInSection;
    }

    public void setWordsInSection(int wordsInSection) {
        this.wordsInSection = wordsInSection;
    }

    public Chapter getChapter() {
        return chapter;
    }

    /**
     * Sets the chapter to which this section belongs.<br>
     * Also tells that chapter, that it owns this section.
     *
     * @param chapter the chapter to tie this section to.
     */
    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
        chapter.addSection(this);
    }

    /**
     * Asks the chapter it is tied to to return the text part of it (the chapter) that contains this section
     *
     * @return all the text for this section.
     */
    public String getSectionText() {
        return chapter.getSectionText(this);
    }
}
