
/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 * 
 * <ul>
 * 
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 * 
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 * 
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 * 
 * </ul>
 * 
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import javax.swing.*;
import java.io.*;
import java.util.Collection;

/**
 * Does not extend a TrieTree, but rather owns one, via the <code>TKD</code> it's based on.
 *
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-18
 */
public class KDB {
    private TrieTree tree;
    private TKD tkd;
    private String currentlySoughtAfterWord = "";
    private int searchPriority = 0;

    /**
     * Constructs the KDB and sets the <code>TKD</code> and <code>TrieTree</code> instances for this class.<br>
     * Usually instansiated from TKD.createKDB();
     *
     * @param tkd the <code>TKD</code> to base the KDB off of.
     * @see TKD#createKDB()
     *
     */
    KDB(TKD tkd) {
        this.tree = tkd.getTree();
        this.tkd = tkd;
    }

    /**
     * Constructs the KDB based on a serialized <code>TKD</code>. Reads the serialised file from disk, creates a tkd, and creates a KDB based on it.<br>
     *
     * @param serializedTkdFilename filename from which to read the tkd
     */
    KDB(String serializedTkdFilename) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(serializedTkdFilename)));
            this.tkd = (TKD) ois.readObject();
            ois.close();
            this.tree = tkd.getTree();
        } catch (InvalidClassException ice) {
            System.out.println("One or more class-definitions have changed since this TKD was created. Please recreate it from source, and try again.");
        } catch (FileNotFoundException fnfe) {
            System.out.println("FILE NOT FOUND!");
        } catch (IOException e) {
            System.out.println("Some other IO error occured, printing stack trace:");
            e.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace(System.out);
        }
    }

    /**
     * Finds the entire word specified in the KDB's <code>TrieTree</code>.
     *
     * @param word Complete word to search for
     * @return A Collection of objects who's keys match <code>word</code> (returns null if not found)
     * @see KDB#searchString(java.lang.String)
     * @see TrieTree#searchWord(java.lang.String)
     *
     */
    public Collection searchWord(String word) {
        return tree.searchWord(word);
    }

    /**
     * Finds all words that have the supplied prefix<br>
     * if (prefix == "cat") then return "catastrohpe", "cat", "cathartic", "cateract", et.c.<br>
     * (useful for populating a list of words based on a certain stem)<br>
     *
     * @param prefix the prefix to find all words for
     * @return A Collection of Strings that all contain the prefix <code>prefix</code> (returns null if nothing is found)
     * @see KDB#searchWord(java.lang.String)
     * @see TrieTree#searchString(java.lang.String)
     */
    public Collection searchString(String prefix) {
        return tree.searchString(prefix);
    }

    /**
     * Starts a new search, updates the current word being sought, and then returns.<br>
     * NOTE: that one KDB cannot support two entirely different searches at a time, ONLY multiple searches started for the same expanding stem.<br>
     * i.e having one window search the kdb for "dog" and another for "cat" will NOT work. However searching for "ca" and "cat" will.<br>
     * Actually, it'll work, but it will not get the desired effect<br>
     * This might be remedied in the future.<br>
     * Not that you'd actually have time or reaction speed enough to start two very different searches, since it completes in under a microseconds, but still.<br>
     *
     * @param word the word being sought
     * @param listBox an instance of JList to populate with the results.
     */
    public void startNewSearch(String word, JList listBox) {

        // this gets updated by the last (latest) method call to this function.
        currentlySoughtAfterWord = word;
        (new WordFinder(this, searchPriority++, word, listBox)).start();
    }

    public String getCurrentlySoughtAfterWord() {
        return currentlySoughtAfterWord;
    }

    public int getSearchPriority() {
        return searchPriority;
    }

    public TKD getTkd() {
        return tkd;
    }

}
