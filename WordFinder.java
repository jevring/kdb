
/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 * 
 * <ul>
 * 
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 * 
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 * 
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 * 
 * </ul>
 * 
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import javax.swing.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Takes a KDB on creation, and houses threads that search the KDB for words based on a certain stem.<br>
 * When the thread has finished executing its search, it'll check certain parameters, and decided if the result <br>
 * returned by THIS instance of the search is still wanted, and  if so, set the results.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-24
 */
public class WordFinder extends Thread {
    private KDB kdb;
    private int priority;
    private String word;
    private JList listBox;

    /**
     * Constructs the thread that will carry out a search in the defined KDB.<br>
     * This can easily be modified to take an array or any other data structure instead of the listbox to house the result, but we don't need that now.
     *
     * @param kdb the <code>KDB</code> to search in
     * @param priority the result priority of the current thread (NOTE: NOT the Thread priority)
     * @param word the word being sought
     * @param listBox the listbox to populate with the result
     */
    public WordFinder(KDB kdb, int priority, String word, JList listBox) {
        this.kdb = kdb;
        this.priority = priority;
        this.word = word;
        this.listBox = listBox;

    }

    /**
     * The method that does the actual work. searches the specified KDB for the specified word, checks the word and priority against that in the KDB, and if this is the same or better, populate the list.<br>
     *
     */
    public void run() {
        // this runs until it's finished
        // we WOULD like to be able to kill searches in "mid-air", but that's an excersise for an actual threaded search-function run on another tree that supports it.
        // besides, there's only G and U here anyway ;)
        Collection c = kdb.searchString(word);
        // NOTE: this returns tree.wordsFromThisStem, which is a linked list of other TrieTree nodes that contain WordReference objects. MIND THE GAP when populating the listbox!

        // we need both priority and word, to make sure we get the latest match, even if a char has been erased and reentered.
        if (word.equals(kdb.getCurrentlySoughtAfterWord())) {
            // this needs to know what object to write the results to! (which GUI object that is)
            HashMap cache = new HashMap();
            // clear it first, just to make sure.
            listBox.removeAll();

            if (c != null) {
                // NOTE: see, want to store WordReference objects, so that we can use .toString() to display the name in the list,
                // and when it's clicked, we already have the WordReference, so that we can call .getOccurances() on it
                // to get the list, from which we later can do getSection() or .getSection().getChapter()

                // get the size of the linked list, and create an array with wordreferences of that size.
                // .size() will execute quickly, since it's a maintaned value, updated on add and remove.

                LinkedList wrs = new LinkedList();
                // get the LinkedList of TrieTree nodes.
                Iterator i = c.iterator();
                while (i.hasNext()) {
                    // loop over the TrieTree nodes and add the nodeWord to the list.
                    TrieTree t = (TrieTree)i.next();
                    // this is actually a list of MANY trie tree heads, what each have a word.

                    // we need to add the nodeWord (one wordreference per trietree) to the list of partial hits
                    Iterator j = t.getReferences().iterator();
                    while (j.hasNext()) {
                        WordReference wr = (WordReference)j.next();
                        // we need this cache here for the DirTKD, that would otherwise show multiple occurances of the same word, IF they were present
                        // in the same dir, but in different chapters (files). This way, the word only shows up once, but the occurances gets added.
                        // cache tested and working.
                        if (cache.containsKey(wr.getWord())) {
                            ((WordReference)cache.get(wr.getWord())).addOccurances(wr.getOccurances());
                        } else {
                            cache.put(wr.getWord(), wr);
                            wrs.addLast(wr);
                        }
                    }
                }
                WordReference[] t = new WordReference[wrs.size()];
                wrs.toArray(t);
                listBox.setListData(t);
                listBox.validate();
            }// otherwise, it should be empty, and it will be, since we've already cleared it at the top
        } // if not, well then we took too long and another, more precise, search beat us to the punch.
    }
}

