/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Date;

/**
 * The part of the GUI that handles the creating of new TKDs.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-05-25
 */
public class AddTKDGui extends JPanel implements ActionListener {
    String source;

    boolean isLocalFile = true;

    // items:
    JButton selectSource;
    JButton createIndex;
    JButton pasteUrl;
    JComboBox numberOfWords;
    JTextField name;
    JTextArea description;

    // containers:
    JPanel headButtonPane;
    JPanel informationPane;
    JPanel nameAndBoxPane;
    JPanel descriptionPane;



    /**
     * Constructs teh control objects and handles events.
     */
    public AddTKDGui() {
        // TODO: enable drag'n'drop (2.0)

        this.setLayout(new BorderLayout());

        selectSource = new JButton("Select Local Source");
        selectSource.addActionListener(this);

        createIndex = new JButton("Create Index and Save File");
        createIndex.addActionListener(this);

        pasteUrl = new JButton("Select Remore Source (via URL, WILL be cached locally)");
        pasteUrl.setToolTipText("This will read the file from the remote location, cache it on disk, and put it in the repository..");
        pasteUrl.addActionListener(this);

        headButtonPane = new JPanel();
        headButtonPane.add(selectSource);
        headButtonPane.add(createIndex);
        headButtonPane.add(pasteUrl);
        headButtonPane.setBorder(BorderFactory.createTitledBorder(" Ye Olde Buttons "));

        this.add(headButtonPane, BorderLayout.NORTH);

        name = new JTextField(20);

        numberOfWords = new JComboBox(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"});
        nameAndBoxPane = new JPanel();
        nameAndBoxPane.add(name);
        nameAndBoxPane.add(numberOfWords);
        nameAndBoxPane.setBorder(BorderFactory.createTitledBorder(" Name and Number Words "));

        description = new JTextArea();
        description.setRows(5);
        description.setColumns(40);
        description.setMargin(new Insets(5,5,5,5));
        description.setFont(new Font("Courier New", Font.PLAIN, 12));
        description.setBorder(BorderFactory.createEtchedBorder());

        descriptionPane = new JPanel(new BorderLayout());
        descriptionPane.add(description, BorderLayout.CENTER);
        descriptionPane.setBorder(BorderFactory.createTitledBorder(" Description "));

        informationPane = new JPanel(new BorderLayout());
        informationPane.add(nameAndBoxPane, BorderLayout.NORTH);
        informationPane.add(descriptionPane, BorderLayout.CENTER);

        this.add(informationPane, BorderLayout.CENTER);
    }

    /**
     * Invoked when an action occurs.
     *
     * @param e the ActionEvent itself
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == selectSource) {
            JFileChooser jfc = new JFileChooser();
            jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
                System.out.println("File location has been accepted");
                source = jfc.getSelectedFile().getAbsolutePath();
                headButtonPane.setBorder(BorderFactory.createTitledBorder(" Chosen Source: " + source + ""));
                isLocalFile = true;
            }
        } else if (e.getSource() == createIndex) {
            JFileChooser jfc = new JFileChooser();
            if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                File f = jfc.getSelectedFile().getAbsoluteFile();
                // save to file:
                boolean okToWriteToFile = true;
                if(f.exists()) {
                    int answer = JOptionPane.showConfirmDialog(this, "The file " + f.getAbsolutePath() + "exists. do you with to overwrite?", "Overwrite file?", JOptionPane.YES_NO_OPTION);
                    // YES_OPION = 0
                    if (answer == 0) {
                        okToWriteToFile = true;
                    } else {
                        okToWriteToFile = false;
                    }
                }
                TKD tkd = null;
                String extraDescription = "\n";
                extraDescription += "Date Added: " + new Date() + "\n";
                // add more relevant information here if we like to

                if (isLocalFile) {
                    File sourceFile = new File(source);
                    extraDescription += "Source: " + sourceFile.getAbsolutePath() + "\n";
                    if (sourceFile.isFile() && sourceFile.exists()) {
                        extraDescription += "TKD Type: FileTKD";
                        tkd = new FileTKD(name.getText(), description.getText() + extraDescription);
                    } else if (sourceFile.isDirectory() && sourceFile.exists()) {
                        extraDescription += "TKD Type: DirTKD";
                        tkd = new DirTKD(name.getText(), description.getText() + extraDescription);
                    } else {
                        System.out.println("file " + sourceFile.getAbsolutePath() + " is either not a file, or has siezed to exist");
                    }
                } else {
                    extraDescription += "Source: " + source + "\n";
                    extraDescription += "TKD Type: URLTKD \n";
                    tkd = new URLTKD(name.getText(), description.getText() + extraDescription);
                }
                if (tkd != null) {
                    System.out.println("Indexing: " + source);
                    long t0 = System.currentTimeMillis();
                    tkd.createIndex(source, Integer.parseInt((String) numberOfWords.getSelectedItem()));
                    long t1 = System.currentTimeMillis();
                    System.out.println("Indexing complete. Time taken: " + ((t1 - t0) / 1000) + " seconds");
                    // if the file doesn't exist, it's automatically ok to write to it.
                    if(okToWriteToFile) {
                        System.out.println("Writing to file.");
                        tkd.saveToFile(f.getAbsolutePath());
                        // kill the object. have the GC transfer it out of memory
                        tkd = null;
                    }
                }
            }
        } else if (e.getSource() == pasteUrl) {
            String url = JOptionPane.showInputDialog("Enter URL");
            source = url;
            isLocalFile = false;
            headButtonPane.setBorder(BorderFactory.createTitledBorder(" Chosen Source (URL): " + url + ""));
        }
    }
    
}