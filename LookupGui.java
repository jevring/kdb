/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

// TODO: word-highlighting in the text display? (need HTML display for that probably) (2.0)
/**
 * The part of the gui that handles the searching in a specified TKD.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-05-25
 */
public class LookupGui extends JPanel implements ActionListener {
    private KDB kdb;
    private Chapter lastChapterDisplayed = null;
    private Section lastSectionDisplayed = null;

    //items:
    JButton selectTKD;
    JButton showTKDInfo;
    JTextField searchWord;
    JList partialResultList;
    JList fullResults;
    JTextArea documentView;

    JPanel radioButtonPane;
    ButtonGroup textPieceToShow;
    JRadioButton showChapter;
    JRadioButton showSection;

    //containers:
    JPanel docViewHolder;
    JScrollPane docViewScroller;
    JPanel searchItemHolder;
    JScrollPane partialResultScroller;
    JScrollPane fullResultsScroller;
    JPanel headButtons;

    // holders for each object to force the layout to look right
    JPanel searchWordHolder;
    JPanel partialResultListHolder;
    JPanel fullResultsHolder;

    JFrame mainWindow;


    /**
     * Constructs the entire search gui and attatches various listerners and event handlers.
     *
     * @param mainWindow the parent window for this object. used to setTitle() on.
     */
    public LookupGui(JFrame mainWindow) {
        this.mainWindow = mainWindow;
        this.setLayout(new BorderLayout());

        //search view
        searchItemHolder = new JPanel();

        searchItemHolder.setLayout(new BoxLayout(searchItemHolder, BoxLayout.Y_AXIS));

        headButtons = new JPanel();
        selectTKD = new JButton("select TKD");
        selectTKD.addActionListener(this);
        headButtons.add(selectTKD);

        showTKDInfo = new JButton("show TKD info");
        showTKDInfo.addActionListener(this);
        headButtons.add(showTKDInfo);

        searchItemHolder.add(headButtons);
        searchItemHolder.add(Box.createVerticalGlue());


        searchItemHolder.add(Box.createRigidArea(new Dimension(0, 5)));

        radioButtonPane = new JPanel(new FlowLayout());
        textPieceToShow = new ButtonGroup();
        showChapter = new JRadioButton("show chapter", false);
        showChapter.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                int answer = JOptionPane.showConfirmDialog(null, "Are you sure you want to display the entire chapter?\n If it's based on a large file, the system may become unresponsive for a shorter period of time", "Are you sure about showing the section?", JOptionPane.YES_NO_OPTION);
                if (answer == 1) {
                    showSection.setSelected(true);
                    // TODO: this responds strangely, check this.
                }
            }
        });
        showSection = new JRadioButton("show section", true);
        textPieceToShow.add(showChapter);
        textPieceToShow.add(showSection);
        radioButtonPane.add(showChapter);
        radioButtonPane.add(showSection);
        radioButtonPane.setBorder(BorderFactory.createTitledBorder(" Select Text Display "));
        searchItemHolder.add(radioButtonPane);
        searchItemHolder.add(Box.createVerticalGlue());

        searchItemHolder.add(Box.createRigidArea(new Dimension(0, 5)));

        // must put each item in it's own JPanel WITHOUT a layout manager, lest we fuck up the overall layout
        searchWordHolder = new JPanel();
        searchWord = new JTextField(20);
        searchWord.addKeyListener(new KeyAdapter() {
            // better to check keyReleased,since by that time, the value of the object will have changed.
            public void keyReleased(KeyEvent ke) {
                if (kdb != null) {
                    kdb.startNewSearch(searchWord.getText(), partialResultList);
                } else {
                    System.out.println("please load a TKD before searching.");
                }
            }
        });
        searchWordHolder.add(searchWord);
        searchItemHolder.add(searchWordHolder);
        searchItemHolder.add(Box.createVerticalGlue());

        partialResultList = new JList();
        partialResultList.setVisibleRowCount(8);
        partialResultList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        partialResultList.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                // do this no matter how many times we clicked, or what mousebutton we used to do it.
                populateFullResultList();
            }
        });
        partialResultList.addKeyListener(new KeyAdapter() {
            // we want this to happen AFTER the key has been pressed, so that we can react to the new selected value. (does it even work that way?)
            public void keyReleased(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_DOWN || ke.getKeyCode() == KeyEvent.VK_UP) {
                    populateFullResultList();
                }
            }
        });
        partialResultScroller = new JScrollPane(partialResultList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        searchItemHolder.add(partialResultScroller);
        searchItemHolder.add(Box.createVerticalGlue());


        //fullResultsHolder = new JPanel();
        fullResults = new JList();
        fullResults.setVisibleRowCount(30);
        fullResults.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        fullResults.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                // do this no matter how many times we clicked, or what moutsebutton we used to do it.
                fillDocumentView();
            }
        });
        fullResults.addKeyListener(new KeyAdapter() {
            // we want this to happen AFTER the key has been pressed, so that we can react to the new selected value. (does it even work that way?)
            public void keyReleased(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_DOWN || ke.getKeyCode() == KeyEvent.VK_UP) {
                    // NOTE: this will absolutely KILL the application if showSelection is checked.!
                    fillDocumentView();
                }
            }
        });
        fullResultsScroller = new JScrollPane(fullResults, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        //fullResultsHolder.add(fullResultsScroller);
        searchItemHolder.add(fullResultsScroller);
        searchItemHolder.setBorder(BorderFactory.createTitledBorder(" Search "));

        this.add(searchItemHolder, BorderLayout.WEST);

        // document view
        docViewHolder = new JPanel(new BorderLayout());
        documentView = new JTextArea();
        documentView.setAutoscrolls(false);
        documentView.setMargin(new Insets(5,5,5,5));
        documentView.setLineWrap(false);
        documentView.setFont(new Font("Courier New", Font.PLAIN, 12));
        documentView.setRows(40);
        documentView.setColumns(80);

        docViewScroller = new JScrollPane(documentView, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        docViewHolder.add(docViewScroller, BorderLayout.CENTER);
        docViewHolder.setBorder(BorderFactory.createTitledBorder(" Document "));

        this.add(docViewHolder, BorderLayout.CENTER);

    }

    /**
     * Invoked when an action occurs.
     */
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == selectTKD) {
            // open a filechooser to select tkd. but right now, we're just going to load one we know exists.
            JFileChooser jfc = new JFileChooser();
            // TODO: add file filters to this to only allow for TKDs to be selected. (2.0)
            if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
                System.out.println("File location has been accepted");
                File f = jfc.getSelectedFile();
                if (f.isFile() && f.exists()) {
                    System.out.println("Loading tkd: " + f.getAbsolutePath());

                    long t0 = System.currentTimeMillis();
                    // TODO: put this into some kind of thread for performance sake. when the thread is running, the gui displays some status. when the thread is done, it tells the gui to display another status. (version 2.0)
                    kdb = new KDB(f.getAbsolutePath());
                    long t1 = System.currentTimeMillis();
                    System.out.println("Done loading tkd. Time taken: " + ((t1 - t0) / 1000) + " seconds");
                } else {
                    System.out.println("file " + f.getAbsolutePath() + " is either not a file, or has sized to exist");
                }
                if (kdb != null) {
                    mainWindow.setTitle(mainWindow.getTitle() + " - Showing: " + kdb.getTkd().getName());
                    // clear all the windows
                    searchWord.setText("");
                    partialResultList.removeAll();
                    fullResults.removeAll();
                    documentView.setText("");
                    lastChapterDisplayed = null;
                    lastSectionDisplayed = null;
                } else {
                    JOptionPane.showMessageDialog(null, "Failed to load TKD from file. Please try again");
                }
            }
        } else if (e.getSource() == showTKDInfo) {
            if (kdb != null) {
                JOptionPane.showMessageDialog(null, "TKD Name: " + kdb.getTkd().getName() + "\n\nTKD Description: \n" + kdb.getTkd().getDescription());
            } else {
                JOptionPane.showMessageDialog(null, "Please load a TKD first...");
            }
        }

    }

    /**
     * Internal function to lessen the cruft in the code.
     *
     */
    private void populateFullResultList() {
        // push the occurances and their words to the fullResultList.
        WordReference wr = (WordReference)partialResultList.getSelectedValue();
        if (wr != null) {
            WordOccurrence[] wos = new WordOccurrence[wr.getOccurances().size()];
            wr.getOccurances().toArray(wos);
            fullResults.setListData(wos);
        }
    }

    /**
     * Internal function to lessen the cruft in the code.
     *
     */
    private void fillDocumentView() {
        // display either chapter or section based on textPieceToShow value
        WordOccurrence wo = (WordOccurrence)fullResults.getSelectedValue();
        if (wo != null) {
            // hmm, how do we highlight words,? both here and in the listings?
            if (showChapter.isSelected()) {
                // show the chapter
                // don't run operations when you don't need to
                if (!wo.getChapter().equals(lastChapterDisplayed)) {
                    // clear the document view
                    documentView.setText("");
                    documentView.append(wo.getChapter().getText());
                    lastChapterDisplayed = wo.getChapter();
                }
            } else if (showSection.isSelected()) {
                // show the section.
                // don't run operations when you don't need to
                if(!wo.getSection().equals(lastSectionDisplayed)) {
                    // clear the document view
                    documentView.setText("");
                    documentView.append(wo.getSection().getSectionText() + "\n");
                    lastSectionDisplayed = wo.getSection();
                }
            }
        }
    }
}