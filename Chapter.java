

/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */
import java.io.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Iterator;

/**
 * Chapter contains a reference ot the original file indexed, or a reference to it's cached counterpart in the repository.<br>
 * NOTE: the repository dir is created in the SAME dir that the application is run.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-13
 */
public class Chapter implements Serializable {
    private String repositoryDirName = "repository/";
    // this keeps the sections in order
    private LinkedList sections;
    private TKD tkd;
    private File repositoryFile;
    private String identifier;

    private int totalLineCount = 0;
    private int totalWordCount = 0;
    private int totalUniqueWordCount = 0;


    /**
     * Constructs the chapter, points to the original file (or caches it and point to the cached file.) and ties it to a TKD.
     *
     * @param identifier the name of the chapter, usually a filename.
     * @param tkd the tkd to connect the chapter to.
     * @param cache true if chapter file is to be cached in local repository, false otherwise
     */
    public Chapter(String identifier, TKD tkd, boolean cache) {
        this.sections = new LinkedList();
        this.tkd = tkd;
        // point the repository to the original file, makes no sense... (what if it is removed)
        repositoryFile = new File(identifier);
        // identifier is needed if we cache it. by caching it, we repoint the repositoryFile pointer to the cached object.
        this.identifier = identifier;
        if (cache) {
            cache();
        }
    }

    /**
     * Constructs a basic non-cached chapter.
     *
     * @param identifier name of chapter, usually a filename
     * @param tkd the tkd to tie the chapter to
     * @see #Chapter(java.lang.String, TKD, boolean)
     */
    public Chapter (String identifier, TKD tkd)  {
        this(identifier, tkd, false);
    }

    /**
     * Caches the file connected to the chapter in a repository tied to the tkd.
     *
     */
    public void cache() {
         try {
            File repository = new File(repositoryDirName);
            if (!repository.exists()) {
                repository.mkdir();
            }

            File originalFile = new File(identifier);
            // make sure we're actually dealing with a file here
            if (originalFile.isFile()) {
                File destinationDir = new File(repositoryDirName + tkd.getName() + "/");
                destinationDir.mkdir();

                File destinationFile = new File(repositoryDirName + tkd.getName() + "/" + originalFile.getName());
                destinationFile.createNewFile();
                this.repositoryFile = destinationFile;
                BufferedReader br = new BufferedReader(new FileReader(originalFile));
                PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(destinationFile)));
                String line;
                while((line = br.readLine()) != null) {
                    pw.println(line);
                }
                pw.close();
                // create a new File
                // read the old file
                // write to the new file
                // (i.e copy the old file to the new file. the new file should reside in the document repository)
                // $REPOSITORY/TKDName/original.filename
            }
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ties a section to this chapter.
     * @param s the section to be tied
     */
    public void addSection(Section s) {
        sections.addLast(s);
    }

    /**
     * Ties multiple sections to this chapter.
     * @param sections a Collection of sections to be tied
     */
    public void addSections(Collection sections) {
        this.sections.addAll(sections);
    }

    /**
     * Gets the text for a section contained in this chapter.<br>
     * Usually only called from a section with chapter.getSection(this)
     *
     * @param section section to get the text for
     * @return the section text
     * @see Section#getSectionText()
     */
    public String getSectionText(Section section) {
        if (!sections.contains(section)) {
            System.out.println("the section " + section + " isn't part of this chapter");
            return null;
        }
        int startingLine = section.getStartingLineInChapter();
        int endLine = startingLine + section.getLinesInSection();
        //System.out.println("start: " + startingLine + " end: " + endLine);
        int i = 0;
        String line;
        // create a quite large StringBuffer to save on resizeing. defaults to 16, and in this case, we'll have WAY more than 16 chars in a seciton
        StringBuffer sb = new StringBuffer(16384);
        try {
            BufferedReader br = new BufferedReader(new FileReader(repositoryFile));
            while ((line = br.readLine()) != null) {
                i++;
                // start adding the lines in the defined section to the section text when we reach past the point of where the section begins
                if (i > startingLine) {
                    sb.append(line + "\n");
                }
                // when we find the end of the seciton, jump out
                if (i >= endLine) {
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return sb.toString();
    }

    /**
     * Gets the <code>File</code> object for the file that contains this chapter. Points to the original file by default, the repository file if cached.
     *
     * @return the file that contains this chapter
     */
    public File getFile() {
        return repositoryFile;
    }

    public String getText() {
        StringBuffer sb = new StringBuffer(32768);
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(repositoryFile));
            while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    // getters and setters, no need to javadoc those.
    public int getTotalLineCount() {
        return totalLineCount;
    }

    public void setTotalLineCount(int totalLineCount) {
        this.totalLineCount = totalLineCount;
    }

    public int getTotalWordCount() {
        return totalWordCount;
    }

    public void setTotalWordCount(int totalWordCount) {
        this.totalWordCount = totalWordCount;
    }

    public int getTotalUniqueWordCount() {
        return totalUniqueWordCount;
    }

    public void setTotalUniqueWordCount(int totalUniqueWordCount) {
        this.totalUniqueWordCount = totalUniqueWordCount;
    }

    public LinkedList getSections() {
        return sections;
    }

    /**
     * a string representation of the chapter.
     *
     * @return returns the filename contianing the chapters text
     */
    public String toString() {
        return repositoryFile.getName();
    }

}
