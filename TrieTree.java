/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import java.io.Serializable;
import java.util.*;

/**
 * <p>Tree type Trie, path-based find structure.<br>
 * Serializable for faster start-up on disk-read.<br>
 * NOTE: all keys are lower-case<br></p>
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-05-12
 *
 */
public class TrieTree implements Serializable {
    // contains the children for this node
    private HashMap children;
    // value of the current node (such as "cat")
    private String nodeWord;
    // A list of references to documents where the word in "value" is found.
    private ArrayList references;
    // the value of the current node
    private String token;
    // contains a list of fully qualified words based on this stem
    private Collection wordsFromThisStem;


    /**
     * Constructs the root node. The root node is the only node that will use this constructor.
     */
    public TrieTree() {
        // root node will be the only one using this constructor where value=null
        this(null, null);
    }

    /**
     * Constructs the child nodes that are trees in themselves.
     *
     * @param token the value of the part of the path this node represents
     * @param nodeWord the value of the path SO FAR that this node represents
     */
    public TrieTree(String token, String nodeWord) {
        this.token = token;
        this.nodeWord = nodeWord;
        children = new HashMap();
        // use HashSet here to avoid adding duplicates
        wordsFromThisStem = new HashSet();

    }

    /**
     * Inserts word <code> word </code> (in lower-case) and ties value <code> value </code> to that node.
     *
     * @param word the word to insert into the tree
     * @param value the object linked to that word
     */
    public void insert(String word, Object value) {
        wordsFromThisStem.addAll( store(word.toLowerCase(), value, 0) );
    }

    /**
     * Internal method used to recurse down into the trees to find the proper place to insert the node.
     *
     * @param word the word to be inserted, to which <code>value</code> will match.
     * @param value the value to be inserted
     * @param pointer current string position pointer
     * @return a LinkedList with full words based on the stem or the currently inserted word. populated when the rekursion is pulling out.
     */
    private Collection store(String word, Object value, int pointer) {
        TrieTree node;
        if (pointer == word.length()) {
            // we've reached as far down as we want, insert the word and let's get the hell out of here

            // we've traversed the entire word. add a stop-symbol, and then add the value to the ArrayList of references
            // that already exists. if no ArrayList exists, create it, and then add to it
            // The only case where it already exists is if we've already encountered the word before, and want to add a
            // reference to this word
            if (!children.containsKey("$")) {
                children.put("$", null);
            }
            node = this;
            if (node.references == null) {
                node.references = new ArrayList();
            }
            // here atleast we want a WordReference, like this, but it's not a general implementation, so change this when you're fucking awake.
            node.references.add(value);
            wordsFromThisStem.add(this);
            return wordsFromThisStem;
            //System.out.println("Added: " + word + " with value: " + value);

        } else {
            // we need to dig deeper, get the correct child, and tell that child to put the object we want into it's
            // own tree of children

            // get the letter from the word that the pointer is currently looking at
            String currentToken = word.substring(pointer, pointer+1);

            // check if the map of children contains an instance of this letter
            // children can't be null if we actually have a correct node, so no need to check.
            if (!children.containsKey(currentToken)) {
                // if it does not, create this tree, and descend into it (after if-statement)
                node = new TrieTree(currentToken, word.substring(0, pointer+1));
                children.put(currentToken, node);
            } else {
                // if this letter exists in the map, get the node and descend into it (after if-statement)
                node = (TrieTree)children.get(currentToken);
            }
            wordsFromThisStem.addAll(node.store(word, value, pointer + 1));
            return wordsFromThisStem;

        }
    }

    /**
     * Finds the entire word specified in the Trie.
     *
     * @param word Complete word to search for
     * @return A Collection of objects who's keys match <code>word</code> (returns null if not found)
     * @see TrieTree#searchString(java.lang.String)
     *
     */
    public Collection searchWord(String word) {
        // from this tree, you can get either suffixes for the word, or the word entries themselves

        TrieTree tree;
        tree = findWord(word);
        if (tree != null) {
            return tree.references;
        } else {
            return null;
        }

    }

    /**
     * Finds all words that have the supplied prefix<br>
     * if (prefix == "cat") then return "catastrohpe", "cat", "cathartic", "cateract", et.c.<br>
     * (useful for populating a list of words based on a certain stem)<br>
     *
     * @param prefix the prefix to find all words for
     * @return A Collection of Strings that all contain the prefix <code>prefix</code> (returns null if nothing is found)
     * @see TrieTree#searchString(java.lang.String)
     */
    public Collection searchString(String prefix) {
        // NOTE: this does NOT work, since this can only find actual words that end in $
        // TrieTree tree = findNode(prefix, 0);

        // increase this limit to >1 for the implementation later?
        if (prefix.length() >= 1) {
            TrieTree tree = findNode(prefix);
            //System.out.println(tree);
            if (tree != null) {
                return tree.wordsFromThisStem;
                // OLD SLOWER METHOD return tree.getWordsFromPrefix(prefix);
            } else {
                return null;
            }
        } else {
            return null;
        }

    }

    /**
     * starter wrapper for findWord().
     *
     * @param prefix prefix to be used
     * @return a <code>TrieTree</code> tree in which the root-node's nodeWord matches <code>word</code>
     * @see TrieTree#findNode(java.lang.String, int)
     */
    private TrieTree findNode(String prefix) {
        return findNode(prefix, 0);
    }

    /**
     * Finds a node that's not necessarily a word (ended with "$") but a prefix/stem.<br>
     * NOTE: searches are CASE-INSENSITIVE<br>
     *
     * @param prefix word to search for, serves as a prefix for the rest of the subtrees below it
     * @param pointer current letter pointer
     * @return a <code>TrieTree</code> that's the root node form the pree where prefix ends
     * @see TrieTree#findNode(java.lang.String)
     */
    private TrieTree findNode(String prefix, int pointer) {
        //System.out.println("looking at: prefix=" + prefix + " pointer=" + pointer + " nodeWord=" + nodeWord );
        if (pointer == prefix.length()) {
            // so, we've made it so far as the prefix, now lets start adding stuff
            if (nodeWord.equalsIgnoreCase(prefix)) {
                return this;
            } else {
                // there are no words based this nodes stem.
                return null;
            }
        } else {
            // we need to dig deeper, get the correct child, and tell that child to look for the object we want in it's
            // own tree of children

            // get the letter from the word that the pointer is currently looking at
            String currentToken = prefix.substring(pointer, pointer+1);
            //System.out.println("currentToken:" + currentToken);
            // if the key is not in children, we have a broken word, and must therefore break
            if (!children.containsKey(currentToken)) {
                return null;
            } else {
                // if this letter exists in the map, get the node and descend into it (after if-statement)
                TrieTree node = (TrieTree)children.get(currentToken);
                return node.findNode(prefix, pointer+1);
            }
        }

    }

    /**
     * Looks inside <code>TrieTree tree</code> and based on prefix <code>prefix</code> finds any possible words in the tree.<br>
     * NOTE: doesn't actually need prefix, that can be taken from the root nodes nodeWord<br>
     *
     * @param prefix the prefix on which to base the search.
     * @return an <code>ArrayList</code> of words based on <code>prefix</code>
     * @deprecated access HashSet for pointers to complete words in subtrees to tree instead
     * @see TrieTree#wordsFromThisStem
     */
    private Collection getWordsFromPrefix(String prefix) {
        ArrayList words = new ArrayList();
        // need a base case for the recursion to stop
        Iterator i = children.keySet().iterator();
        while (i.hasNext()) {
            Object o = children.get(i.next());
            //System.out.println(o);

            if (o == null) {
                // o will be null when the key is "$"
                words.add(nodeWord);
                //System.out.println("found word: " + nodeWord);

            } else {
                TrieTree tt = (TrieTree)o;
                // recurse over all the subtrees in the forest
                words.addAll(tt.getWordsFromPrefix(tt.nodeWord));
            }
        }
        return words;
    }

    /**
     * starter wrapper for findWord().
     *
     * @param word the word to be found
     * @return a <code>TrieTree</code> tree in which the root-node's nodeWord matches <code>word</code>
     * @see TrieTree#findNode(java.lang.String, int)
     */
    private TrieTree findWord(String word)  {
        return findWord(word, 0);
    }

    /**
     * internal find method, used for recursing into the tree to find a forrest at the end of the sought-after word.
     *
     * @param word the sought-after word
     * @param pointer the current letter pointer for the word
     * @return a Collection of TrieTree entries.
     * @see TrieTree#findNode(java.lang.String)
     */
    private TrieTree findWord(String word, int pointer)  {

        if (pointer == word.length()) {
            // returns the current node, from which WordReferences and/or trees can be extracted
            if(children.containsKey("$")) {
                //System.out.println("found word: " + word);
                return this;
            } else {
                //System.out.println("found NOTHING");
                //throw exception instead?
                return null;
            }
        } else {
            // we need to dig deeper, get the correct child, and tell that child to look for the object we want in it's
            // own tree of children

            // get the letter from the word that the pointer is currently looking at
            String currentToken = word.substring(pointer, pointer + 1);

            // if the key is not in children, we have a broken word, and must therefore break
            if (!children.containsKey(currentToken)) {
                return null;
            } else {
                // if this letter exists in the map, get the node and descend into it (after if-statement)
                TrieTree node = (TrieTree)children.get(currentToken);
                return node.findWord(word, pointer + 1);
            }
        }
    }

    /**
     * shows a string representation of the object. easily parsable.
     *
     * @return a string representation of the object
     */
    public String toString() {
        return this.getClass().toString() + "[nodeWord=" + nodeWord + ";token=" + token + ";children=" + children.keySet().toString() + "]";
    }

    /**
     * Gets the node word for the root node of this tree.
     * @return the word this node represents 
     */
    public String getNodeWord() {
        return nodeWord;
    }

    public ArrayList getReferences() {
        return references;
    }

}