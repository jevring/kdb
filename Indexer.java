/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */ 

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Abstract base class for all our Indexers.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-12
 */

public abstract class Indexer implements Serializable {
    protected HashSet stopWords;
    protected HashMap wordReferences;
    protected TrieTree tree;
    protected int numClosestWords;
    protected WordOccurrenceCircularQueue wocq;

    /**
     * Constructs the indexer.
     *
     * @param tree the tree (of a certain kind) that will contain the WordReferences
     * @param numClosestWords the number of words before ad after a specified word to archive.
     */
    public Indexer(TrieTree tree, int numClosestWords) {
        this.tree = tree;
        this.numClosestWords = numClosestWords;
        wocq = new WordOccurrenceCircularQueue((numClosestWords * 2 + 1));
        stopWords = new HashSet();
        addStopWords();
    }

    /**
     * Internal method used to populare a set of "stop words".
     */
    private void addStopWords() {
        // english
        stopWords.add("a");
        stopWords.add("and");
        stopWords.add("the");
        stopWords.add("of");
        stopWords.add("if");
        stopWords.add("at");
        stopWords.add("to");
        stopWords.add("or");
        stopWords.add("is");
        stopWords.add("the");
        stopWords.add("by");

        // swedish
        stopWords.add("som");
        stopWords.add("att");
        stopWords.add("�r");
        stopWords.add("i");
        stopWords.add("p�");
        stopWords.add("en");
        stopWords.add("ett");
        stopWords.add("den");
        stopWords.add("det");
        stopWords.add("dom");
        stopWords.add("de");
        stopWords.add("dem");

    }

    /**
     * Does the actual reading. Implemented in all children.
     */
    public abstract void read();
}
