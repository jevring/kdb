# KDB
It's been a while, so I've forgotten what this acronym stands for.
This was a school project in which we would index bodies of text and provide search
for groups, and we'd return the N words on each side of that word.

Not only that, but we wanted typing suggestions on the fly, which we accomplished using a 
[trie](https://en.wikipedia.org/wiki/Trie)

This is a gui app that'll let you generate such databases and use previously
generated ones.