
/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import java.io.File;

/**
 * A special case of TKD for handling entire dirs of files (chapters).
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-13
 */
public class DirTKD extends TKD{

    /**
     * Constructs the TKD.
     *
     * @param name the name of the TKD such as "the bible". This is also the filename used for saving the tkd to disk. MUST be single-line
     * @param description a multi-line description of the TKD 
     */
    public DirTKD(String name,String description) {
        super(name, description);
    }

    /**
     * Runs over the resources specified and actually creates the index.
     *
     * @param dirname the dirname that contains the files (files as chapters) to be indexed
     * @param numClosestWords the number of words to use
     */
    public void createIndex(String dirname, int numClosestWords) {

        tree = new TrieTree();
        File dir = new File(dirname);
        // if it's not a dir, something is wrong.
        if (dir.isDirectory()) {
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                String ext = file.getName().substring(file.getName().lastIndexOf(".") + 1, file.getName().length());
                System.out.println(ext);
                //check to see that we're only indexing actual text files.
                if (ext.equals("txt")) {
                    FileIndexer fi = new FileIndexer(this, numClosestWords, file.getAbsolutePath(), false);
                    fi.read();
                }
            }
        } else {
            System.out.println("The directory you specified is not a directory: " + dir.getAbsolutePath());
        }
    }
}
