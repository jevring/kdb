
/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 * 
 * <ul>
 * 
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 * 
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 * 
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 * 
 * </ul>
 * 
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */

import java.io.*;
import java.net.URL;

/**
 * TKD version that handles URLs. CUrrently caches them to disk, then treats them like files. Also tells the chapter to do a repository cache of them.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-26
 */
public class URLTKD extends TKD {

    /**
     * Constructs the basic TKD.
     *
     * @param name
     * @param description
     * @see TKD#TKD(java.lang.String, java.lang.String)
     */
    public URLTKD(String name, String description) {
        super(name, description);
    }

    /**
     * Create the indexed tree that's needed for this to be useful.
     *
     * @param resourceName    what to index.
     * @param numClosestWords number of words to be indexed
     */
    public void createIndex(String resourceName, int numClosestWords) {
        // read this file to ./tmp/somefile, and then cache it via the chapter reopsitory cache.
        //BufferedReader br = new BufferedReader(new InputStreamReader(new URL("http://bleh.com/apa.txt").openStream()));
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new URL(resourceName).openStream()));
            File tempDir = new File("tmp");
            if (!tempDir.exists()) {
                tempDir.mkdir();
            }
            String tmpFileStore = "tmp/urlCacheFile.txt";
            File destinationFile = new File(tmpFileStore);
            // just in case it got left there the last time. failed execution or the liek
            if (destinationFile.exists()) {
                destinationFile.delete();
            }
            destinationFile.createNewFile();
            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(destinationFile)));
            String line;
            while((line = br.readLine()) != null) {
                pw.println(line);
            }
            pw.close();
            tree = new TrieTree();
            indexer = new FileIndexer(this, numClosestWords, tmpFileStore, true);
            indexer.read();
            // when we're done with the tmp file, remove it
            destinationFile.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
