
/**
 * Copyright (c) 2004, Markus Jevring <markus@jevring.net>
 * All rights reserved.</p>
 *
 * <p>Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:</p>
 *
 * <ul>
 *
 * 	<li>Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.</li>
 *
 * 	<li>Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.</li>
 *
 * 	<li>Neither the name of the University nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.</li>
 *
 * </ul>
 *
 * <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
 */
import java.io.*;
/**
 * Base TKD class that all children inherit from. Different types of TKDs can be written, for virtually any media, but they all need to inherit from this one.
 *
 * @author Markus Jevring <maje4823@student.uu.se>
 * @version 2004-maj-13
 */
public abstract class TKD implements Serializable {
    TrieTree tree;
    Indexer indexer;
    protected String name;
    protected String description;

    /**
     * Constructs a basic TKD. Children call up to do this.
     *
     * @param name name of the TKD
     * @param description description of the TKD
     */
    public TKD(String name, String description){
        this.name = name;
        this.description = description;
        tree = null;
    }

    /**
     * Gets the <code>TrieTree</code> that contains all the <code>WordReferences</code> for this TKD.<br>
     * NOTE: if you have NOT already called .createIndex(String, String) on the child TKD, this will be null.
     *
     * @return a tree full or references to words in secitons in chapters.
     */
    public TrieTree getTree() {
        return tree;
    }

    /**
     * Placeholder to force children to create the indexed tree that's needed for this to be useful.
     *
     * @param resourceName what to index.
     * @param numClosestWords number of words to be indexed
     */
    public abstract void createIndex(String resourceName, int numClosestWords);

    /**
     * Takes this tkd (or one inhereted from it), serializes it, and saves it to disk.<br>
     * NOTE: these files are considerately much larger than the amount of text they map. considerately.
     *
     * @param filename where to save the tkd to
     */
    public void saveToFile(String filename) {
        File saveFile = new File(filename);
        // NOTE: looks like the growth of this file is linear
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(saveFile));
            // write this tkd to file
            oos.writeObject(this);
            oos.close();
            System.out.println("TKD written to file: " + filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method created to support the restriction "a TKD CREATES a KDB".
     *
     * @return the KDB based on this TKD
     * @see KDB#KDB(TKD) 
     */
    public KDB createKDB() {
        return new KDB(this);
    }

    // getters and setters, no need to javadoc those
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
